-include Makefile.properties

build: check.variables
	@docker build \
		-t $(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		.

publish: build check.variables
	@$(MAKE) version.get | grep -v "make" > $(CURDIR)/.publish.version
	@docker tag \
		$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		$(IMAGE_REGISTRY_URL)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):$$(cat $(CURDIR)/.publish.version)
	@$(MAKE) log.info MSG="Publishing $(IMAGE_NAMESPACE)/$(IMAGE_NAME):$$(cat $(CURDIR)/.publish.version) to $(IMAGE_REGISTRY_URL)..."
	-@docker push \
		$(IMAGE_REGISTRY_URL)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):$$(cat $(CURDIR)/.publish.version)
	@rm -rf $(CURDIR)/.publish.version
	@docker tag \
		$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		$(IMAGE_REGISTRY_URL)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest
	@$(MAKE) log.info MSG="Publishing $(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest to $(IMAGE_REGISTRY_URL)..."
	@docker push \
		$(IMAGE_REGISTRY_URL)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest

multiarch.build.publish: check.variables
	@docker buildx create --use
	@$(MAKE) version.get | grep -v "make" > $(CURDIR)/.publish.version
	@docker buildx build \
		-t $(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest \
		-t $(IMAGE_REGISTRY_URL)/$(IMAGE_NAMESPACE)/$(IMAGE_NAME):$$(cat $(CURDIR)/.publish.version) \
		--platform linux/amd64,linux/arm64 --push .
	@$(MAKE) log.info MSG="Publishing $(IMAGE_NAMESPACE)/$(IMAGE_NAME):latest to $(IMAGE_REGISTRY_URL)..."
	@$(MAKE) log.info MSG="Publishing $(IMAGE_NAMESPACE)/$(IMAGE_NAME):$$(cat $(CURDIR)/.publish.version) to $(IMAGE_REGISTRY_URL)..."

version.bump:
	@docker run \
		-v "$(CURDIR):/app" \
		public.ecr.aws/govtechsg/semver:latest \
		iterate ${VERSION} -i

version.get:
	@docker run \
		-v "$(CURDIR):/app" \
		public.ecr.aws/govtechsg/semver:latest \
		get-latest -q

check.variables:
	@if ! [ -f "./Makefile.properties" ]; then \
		$(MAKE) log.warn MSG="No Makefile.properties found. Using sample.properties to generate one now..."; \
		cp $(CURDIR)/sample.properties $(CURDIR)/Makefile.properties; \
		$(MAKE) log.info MSG="Done. Run the command again."; \
	fi

log.debug:
	-@printf -- "\033[36m\033[1m_ [DEBUG] ${MSG}\033[0m\n"
log.info:
	-@printf -- "\033[32m\033[1m>  [INFO] ${MSG}\033[0m\n"
log.warn:
	-@printf -- "\033[33m\033[1m?  [WARN] ${MSG}\033[0m\n"
log.error:
	-@printf -- "\033[31m\033[1m! [ERROR] ${MSG}\033[0m\n"