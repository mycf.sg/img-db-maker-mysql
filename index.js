const crypto = require('crypto');

const Case = require('case');
const express = require('express');
const knex = require('knex');
const { Client } = require('pg');

const app = express();

const ERR_DB_EXISTS = "DB Exists";
const ERR_USER_EXISTS = "User Exists";

app.use('/:clientName/:host/:rootPassword/:databaseName/:databaseUser/:databasePassword/:databasePrivilege?', (req, res) => {
  const {
    clientName,
    host,
    rootPassword,
    databaseName,
    databaseUser,
    databasePassword,
    databasePrivilege,
  } = req.params;
  const dbName = Case.snake(databaseName);
  const dbUser = Case.snake(databaseUser);
  const dbPrivilege = databasePrivilege || 'ALL';
  switch(clientName) {
  case 'mysql':
  case 'mysql2':
    const database = knex({
      client: clientName,
      connection: {
        user: 'root',
        password: rootPassword,
        host,
      },
    });
    database.raw(`CREATE DATABASE IF NOT EXISTS ${dbName} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci`)
      .then(() =>
        database.raw(`CREATE USER IF NOT EXISTS '${dbUser}'@'%' IDENTIFIED BY '${databasePassword}'`)
      ).then(() =>
        database.raw(`GRANT ${dbPrivilege} ON ${dbName}.* TO '${dbUser}'@'%'`)
      ).then(() => {
        res
          .status(200)
          .json({
            dbName,
            dbUser,
            dbPass: crypto.createHash('md5').update(databasePassword).digest("hex"),
            dbPrivilege,
          });
      }).catch((err) => {
        res
          .status(500)
          .json(err);
      });
  break;
  case 'pg':
    const client = new Client({
      user: 'user',
      host: host,
      database: 'postgres',
      password: rootPassword,
      port: 5432,
    });
    client.connect();
    client.query(`SELECT datname FROM pg_database where datname = '${dbName}'`)
    .then((dbRes) => {
      if (dbRes.rows.length > 0) throw new Error(ERR_DB_EXISTS);
      client.query(`CREATE DATABASE ${dbName}`)
    }).then(() => {
      return client.query(`SELECT rolname FROM pg_roles WHERE rolname = '${dbUser}'`);
    }).then((dbRes) => {
      if (dbRes.rows.length > 0) throw new Error(ERR_USER_EXISTS);
      client.query(`CREATE USER ${dbUser} WITH PASSWORD '${databasePassword}'`);
    }).then(() => {
      client.query(`GRANT ${dbPrivilege} ON DATABASE ${dbName} TO ${dbUser}`)
    }).then(() => {
      res
        .status(200)
        .json({
          dbName,
          dbUser,
          dbPass: crypto.createHash('md5').update(databasePassword).digest("hex"),
          dbPrivilege,
      });
    }).catch((err) => {
      if (err.message == ERR_DB_EXISTS) {
        res.status(200)
        .json({
          dbName,
          msg: "DB already exists, skipping creation",
        });
      } else if (err.message == ERR_USER_EXISTS) {
        res.status(200)
        .json({
          dbName,
          msg: "User already exists, skipping creation",
        });
      } else {
        res
        .status(500)
        .json(err);
      }
    });
  break;
  default:
    res
      .status(400)
      .json('invalid client');
  }
});

const server = app.listen(process.env.PORT);
server.on('listening', () => {
  console.info(`Started on http://localhost:${server.address().port}`);
});
