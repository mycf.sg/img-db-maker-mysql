# DB Maker (MySQL and Postgres)
Service for creating new databases in Docker environments. This service exists to overcome the limitations of the MySQL and Postgres Docker image which can only create a single database. Call this service via [the `db-ensurer` service](https://gitlab.com/mycf.sg/img-db-ensurer-mysql) to provision a new database and user.

# Usage

See the [docker-compose.yml](./docker-compose.yml) for a simple setup.

## `GET /:clientName/:host/:rootPassword/:databaseName/:databaserUser/:databasePassword`

`clientName` can be either `"mysql"`, `"mysql2"`, or `"pg"`.

`host` is the hostname of your MySQL database.

`rootPassword` is the root password for the MySQL database.

`databaseName` is the name of the database you wish to create.

`databaseUser` is the user of the database you wish to create.

`databasePassword` is the password for the user specified in `:databaseUser`.

# Configuration

| Environment Variable | Description | Default |
| --- | --- | --- |
| PORT | Port which the service will listen on, if none is specified, a random open port will be used | `null` |

# Development

## Version Bumping
To bump a version, [run a pipeline](https://gitlab.com/mycf.sg/img-db-maker-mysql/pipelines/new) with the input variable `VERSION_BUMP_TYPE` set to one of `"major"` or `"minor"`. It defauilts to `"patch"`.

# Re-Use

## Pipeline Configuration

| Pipeline Variable | Description | Example Value |
| --- | --- | -- |
| IMAGE_NAME | Name of the Docker image (eg. for DockerHub, docker.io/org/**__image__**:tag) | `"db-maker-mysql"` |
| IMAGE_NAMESPACE | Namespace of the Docker image (eg. for DockerHub, docker.io/**__org__**/image:tag) | `"mycfsg"` |
| IMAGE_REGISTRY_PASSWORD | Password for the registry user identified in `IMAGE_REGISTRY_USERNAME` | `"password"` |
| IMAGE_REGISTRY_URL | URL of the image registry without the http/https protocol prefix | `"docker.io"` |
| IMAGE_REGISTRY_USERNAME | Username of the registry user | `"user"` |
| SSH_DEPLOY_KEY | Base64 encoded private key corresponding to a registered Deploy Key (see below section on Deploy Key Setup) | `null` |

## Deploy Key Setup
Generate a key using:

```bash
ssh-keygen -t rsa -b 8192 -f ./img-db-maker-mysql -q -N ""
```

Run the private key through a base64 encoding and copy the output:

```bash
cat ./img-db-maker-mysql | base64 -w 0
```

Paste the value in the pipeline variable `SSH_DEPLOY_KEY`.

Copy the contents of the public key and paste the value in the Deploy Keys section of the CI/CD->Repository settings page in your repository.

# License
This project is licensed under [the MIT license](./LICENSE).
